#!/bin/sh

IMAGE_NAME=$REGISTRY/build-tools:$CI_PIPELINE_ID

buildah bud -t $IMAGE_NAME .
buildah login -u AWS -p $REGISTRY_PASSWORD $REGISTRY
buildah push $IMAGE_NAME
