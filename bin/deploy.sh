#!/bin/sh

IMAGE_NAME=$REGISTRY/build-tools

skopeo login -u AWS -p $REGISTRY_PASSWORD $REGISTRY

skopeo copy docker://$IMAGE_NAME:$CI_PIPELINE_ID docker://$IMAGE_NAME:latest
