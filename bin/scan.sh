#!/bin/sh

HOME=/tmp
IMAGE_NAME=$REGISTRY/build-tools:$CI_PIPELINE_ID

mkdir /tmp/.docker

echo $DOCKER_AUTH_CONFIG > /tmp/.docker/config.json

clairctl report --host http://paas.mkdev.me:80 $IMAGE_NAME > report

cves=$(cat report | grep " found " | wc -l)

if [ $cves -gt 0 ]
then
  cat report
  exit 1
fi
