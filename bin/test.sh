#!/bin/sh

IMAGE_NAME=$REGISTRY/build-tools:$CI_PIPELINE_ID

curl -L https://github.com/wagoodman/dive/releases/download/v0.9.2/dive_0.9.2_linux_amd64.tar.gz -o dive.tar.gz

tar -xvf dive.tar.gz

podman login -u AWS -p $REGISTRY_PASSWORD $REGISTRY

podman pull $IMAGE_NAME

./dive podman://$IMAGE_NAME
